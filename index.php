<!DOCTYPE html>
<html>
  <?php include("Layout/header.php") ?>
  <!-- Header css meta -->
<body class="" style="background-color: #f1f1f1">
  <div class="">
  <!-- navbar -->
  <?php include("Layout/nav.php") ?>
  <!-- Sidebar -->
   <section class="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-4"></div>
          <div class="col-sm-4 p-0 mt-5">
             <form class="needs-validation" id="sample_form_id" novalidate>
               
            <div class="card">
              <div class="card-header bg-success text-center">
                  <img src="webroot/img/logo.png" class="img-fluid" width="100" alt="">
                  <div class="h3 bold">CIRMS</div>                  
                  <div class="h6 bold">BARANGAY ESTRELLA SAN MATEO ISABELA</div>                  
              </div>
              <div class="card-body">
                  <div class="form-row">
                    <div class="form-group col-sm-12">
                      <label>Username </label>
                      <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                      <div class="invalid-feedback" id="err_username"></div>
                    </div>
                    <div class="form-group col-sm-12">
                      <label>Password </label>
                      <input type="password" id="password" name="password" placeholder="Password" class="form-control " required>
                      <div class="invalid-feedback" id="err_password"></div>
                    </div>

                    <div class="col-sm-12 text-right">
                     <button class="btn btn-success" type="submit">Login</button>
                    </div>
                  </div>
              </div>
              <div class="card-footer text-right">
              </div>
            </div>
            </form>
          </div>
          <div class="col-sm-4"></div>
        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("Layout/footer.php") ?>
</html>

<script type="text/javascript">
  $("#sample_form_id").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // alert(mydata);
    $.ajax({
      type:"POST",
      url:url + '?action=login',
      data:mydata,
      dataType:'json',
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          // console.log(response);
        if(response.status == true){
          let user = response.user.user_type;
          // if (user == 1) {
          //   window.location = "User/admin.php";
          // }else if (user == 2) {
          //   window.location = "User/midwife.php";
          // }else if (user == 3) {
          //   window.location = "User/health_wo";
          // }
             window.location = "User/";
          showValidator(response.error,'sample_form_id');
        }else{
          if (typeof response.message == 'undefined') {
          
          }else{
            swal("Error",response.message, "error");

          }
          // console.log(response.message);
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });


</script>

