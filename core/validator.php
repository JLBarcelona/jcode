<?php 
class Validator{
	public $fail = false;
	public $message = array();
	public $count = 0;
	public function make($arr, $param){
		$keys = array();
		$data = array();
		$newdata = array();
		foreach ($arr as $k => $v) {
			foreach ($param as $kk => $vv) {
				if ($k == $kk) {
					$keys[] = $kk;
					$array = explode('|', $vv);
			 		foreach ($array as $key) {
			 			$msg = $this->check_rule($k, $v, $key);
				 		if (!empty($msg)) {
				 			$param[$kk] = $msg;
				 		}else{
				 			unset($param[$kk]);
				 		}
			 		}
				}
			}
		}

		// return count($arr);
		if (count($param) > 0) {
			$this->fail = false;
			$this->message = $param;
		}else{
			$this->fail = true;
			$this->message = [];
		}
	}



	public function rule($key,$val,$rule){
		switch ($rule) {
			case 'required':
				return (empty($val)) ? $key.' '.$this->valid_list()[$rule] : false;
			break;
			case 'numeric':
		
			break;
			case 'email':
				return (!filter_var($val, FILTER_VALIDATE_EMAIL)) ? $this->valid_list()[$rule] : false;
			break;
			
			default:
				# code...
			break;
		}
	}


	public function check_rule($key, $val, $rule){
		$data = array();
		$message = $this->rule($key, $val, $rule);
		if (!$message) {
	
		}else{
			$data[] = $message;
		}
		return json_decode(json_encode($data));
	}

	public function valid_list(){
		return array(
			"required" => "is required",
			"numeric" => "must be a number",
			"email" => "Please input valid email address!",
			"unique" => "is already exist!",
		);
	}
}