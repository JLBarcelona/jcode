<?php if (Auth::check()): ?>

<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin') }}" class="brand-link">
      <img src="webroot/img/logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
           <img src="webroot/img/img.png" style="background-image: url(webroot/img/avatar.png);" class="img-bg profile_pict img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Name</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/system/settings') }}" class="nav-link {{ (request()->is('system/settings')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-baby"></i>
              <p>
                Children's Profile
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview {{ (request()->is('employees') || request()->is('employees/payroll')) ? 'menu-open' : '' }} ">
            <a href="#" class="nav-link {{ (request()->is('employees') || request()->is('employees/payroll')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Account
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('employees') }}" class="nav-link {{ (request()->is('employees')) ? 'active' : '' }}">
                  <i class="fas fa-user-nurse nav-icon"></i>
                  <p>Midwife</p>
                </a>
              </li>
              <li class="nav-item active">
                <a href="{{ url('employees/payroll') }}" class="nav-link {{ (request()->is('employees/payroll')) ? 'active' : '' }}">
                  <i class="fas fa-user-md nav-icon"></i>
                  <p>Health Worker</p>
                </a>
              </li>
            </ul>
          </li>
          <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <?php else: ?>
<?php endif ?>
