<head>
	<?php require_once("core/validator.php"); ?>
	<?php require_once('config/auth.php') ?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- <link rel="icon" type="icon/png" href="{{ $icon }}"> -->
	<link rel="stylesheet" href="webroot/plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="webroot/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="webroot/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="webroot/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="webroot/css/main.css">
	<link rel="stylesheet" href="webroot/css/animate.css">
	<link rel="stylesheet" href="webroot/css/sweetalert.css">
	<link rel="stylesheet" href="webroot/css/themes/twitter/twitter.css">
	<link rel="stylesheet" href="webroot/css/main.css">

	<?php if (Auth::check()): ?>
		<?php header('location:User/') ?>
	<?php endif ?>
</head>
