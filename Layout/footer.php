<script src="webroot/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="webroot/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="webroot/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="webroot/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="webroot/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="webroot/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="webroot/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="webroot/dist/js/demo.js"></script>
<script src="webroot/js/main.js"></script>
<script src="webroot/js/sweetalert.min.js"></script>
