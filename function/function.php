<?php 
require_once("../config/config.php");
require_once("../config/auth.php");
require_once("../core/validator.php");

 
$db = new db();

$action = $_REQUEST['action'];

switch ($action) {
	case 'login':
		$v = new Validator();

		 $v->make($_POST, [
		 	'username' => 'required',
		 	'password' => 'required'
		 ]);

		 $data = array('username' => $_POST['username'], 'password' => $_POST['password']);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
			 	echo $db->login('tbl_users', $data);
		 }
	break;

	case 'finish_all_check':
		$selected_child = $_POST['selected_child'];
		$date = date('Y-m-d H:i:s');

		$i = 0;
		foreach ($selected_child as $ids) {
			$data = array('appointment_id' => $ids, 'is_finished' => $date);
	 		if ($db->update('tbl_appointment', $data)) {
	 			$i++;
	 		}
		}
		
		$message = $i.' Schedule marked as finished successfully!';
		if ($i > 0) {
			echo $db->json(['status' => true, 'message' => $message]);	
		}else{
			echo $db->json(['status' => false, 'message' => 'No Item selected!']);	
		}
	break;

	case 'delete_all_check':
		$selected_child = $_POST['selected_child'];
		$i = 0;
		foreach ($selected_child as $ids) {
			$array = array('appointment_id' => $ids);
			if ($db->delete('tbl_appointment', $array)) {
				$i++;
			}
		}
		
		$message = $i.' Schedule deleted successfully!';
		if ($i > 0) {
			echo $db->json(['status' => true, 'message' => $message]);	
		}else{
			echo $db->json(['status' => false, 'message' => 'No Item selected!']);	
		}
	break;

	case 'find_user':
		$id = $_GET['id'];
		$array = array('id' => $id);
		$query = "SELECT * from tbl_users where user_id = :id order by user_id desc";
		$res = $db->find($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'delete_midwife':
		$array = array('user_id' => $_POST['id']);
		if ($db->delete('tbl_users', $array)) {
			echo $db->json(['status' => true, 'message' => 'midwife successfully deleted!']);
		}
	break;

	case 'delete_schedule':
		$array = array('appointment_id' => $_POST['id']);
		if ($db->delete('tbl_appointment', $array)) {
			echo $db->json(['status' => true, 'message' => 'Schedule successfully deleted!']);
		}
	break;

	case 'mark_finish_schedule':
		$date = date('Y-m-d H:i:s');
		$data = array('appointment_id' => $_POST['id'], 'is_finished' => $date);
 		if ($db->update('tbl_appointment', $data)) {
 			echo $db->json(['status' => true, 'message' => 'Record has updated successfully!']);
 		}
	break;

	case 'get_schedule':
		$status = 'On Time';
		$new_data = array();
		$array = array();
		$query = "SELECT DATE(NOW()) as today,a.description,a.appointment_id,a.is_finished as status,b.gender, b.contact_number,DATE(a.appointment_date) as date_appoinment, concat(b.lastname,' ', b.firstname) as fullname,b.middlename from tbl_appointment a
				 	INNER JOIN tbl_child b on a.child_id=b.child_id ";
		$res = $db->get($array,$query);

		foreach ($res as $row) {
			if(empty($row->status)){
				$row->status_msg = 'No Appearance';
			}elseif (strtotime($row->today) > strtotime($row->date_appoinment)) {
				$row->status_msg = 'Late';
			}else{
				$row->status_msg = $status;
			}

			$row->dt_app = date('M d, Y (l)', strtotime($row->date_appoinment));

			$new_data[] = $row;
		}

		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_all_on_sched':
		$date_sched = $_GET['date_sched'];
		$status = 'On Time';
		// echo $date_sched;
		$new_data = array();
		$array = array('date_sched' => $date_sched);
		$query = "SELECT DATE(NOW()) as today,a.description,a.appointment_id,a.is_finished as status,b.gender, b.contact_number,DATE(a.appointment_date) as date_appoinment, concat(b.lastname,' ', b.firstname) as fullname,b.middlename from tbl_appointment a
				 	INNER JOIN tbl_child b on a.child_id=b.child_id 
				 	where DATE(a.appointment_date) = DATE(:date_sched)";
		$res = $db->get($array,$query);

		foreach ($res as $row) {
			if(empty($row->status)){
				$row->status_msg = 'No Appearance';
			}elseif (strtotime($row->today) > strtotime($row->date_appoinment)) {
				$row->status_msg = 'Late';
			}else{
				$row->status_msg = $status;
			}
			$new_data[] = $row;
		}

		$title = date('F d, Y D', strtotime($date_sched));
		echo $db->json(['status' => true, 'data' => $res, 'title' => $title]);
		# code...
	break;

	case 'health_worker_list':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 3 order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'midwife_list':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 2 order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'add_user':
		 $user_what = ($_POST['user_type'] == 2) ? 'Midwife' : 'Health Worker';

		 $v = new Validator();

		 $v->make($_POST, [
		 	'firstname' => 'required',
		 	'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'address' => 'required',
			'username' => 'required',
			'password' => 'required',
			'security_question1' => 'required',
			'security_answer1' => 'required',
			'security_question2' => 'required',
			'security_answer2' => 'required'
		 ]);

		 if (!$v->fail) {
			 echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['user_id'])) {
		 	  $data = array("user_id" => $_POST['user_id'],
							"firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"user_type" => $_POST['user_type'],
							'security_question1' => $_POST['security_question1'],
							'security_answer1' => $_POST['security_answer1'],
							'security_question2' => $_POST['security_question2'],
							'security_answer2' => $_POST['security_answer2'],
							'updated_at' => date('Y-m-d H:i:s A'));

		 		if ($db->update('tbl_users', $data)) {

		 			echo $db->json(['status' => true, 'message' => $user_what.' account updated successfully!']);
		 		}
		 	}else{
		 		$data = array("firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"password" => password_hash($_POST['password'], PASSWORD_DEFAULT),
							"user_type" => $_POST['user_type'],
							'security_question1' => $_POST['security_question1'],
							'security_answer1' => $_POST['security_answer1'],
							'security_question2' => $_POST['security_question2'],
							'security_answer2' => $_POST['security_answer2'],
							'created_at' => date('Y-m-d H:i:s A'));

		 		if ($db->save('tbl_users', $data)) {
		 			echo $db->json(['status' => true, 'message' => $user_what.' account saved successfully!']);
		 		}
		 	}
		 }
	break;

	case 'update_user':
		 $user_what = ($_POST['user_type'] == 2) ? 'Midwife' : 'Health Worker';

		 $v = new Validator();
		 
		 $v->make($_POST, [
		 	'firstname' => 'required',
		 	'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'address' => 'required',
			'username' => 'required',
		 ]);

		 if (!$v->fail) {
			 echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['user_id'])) {
		 	  $data = array("user_id" => $_POST['user_id'],
							"firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"user_type" => $_POST['user_type'],
							'updated_at' => date('Y-m-d H:i:s A'));

		 		if ($db->update('tbl_users', $data)) {
		 			echo $db->json(['status' => true, 'message' => $user_what.' account updated successfully!']);
		 		}
		 	}
		 }
	break;

	
	case 'children_list':
		$array = array();
		$query = "SELECT * from tbl_child order by child_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;


	case 'add_child':
		$v = new Validator();

		 $v->make($_POST, [
		 	'firstname' => 'required',
			'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'purok' => 'required',
			'street' => 'required',
			'kind_of_birth' => 'required',
			'mother_name' => 'required',
			'father_name' => 'required',
			'contact_number' => 'required'
		 ]);


		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['child_id'])) {
		 		$data = array(
		 		'child_id' => $_POST['child_id'],
		 		'firstname' => $_POST['firstname'],
				'middlename' => $_POST['middlename'],
				'lastname' => $_POST['lastname'],
				'birthdate' => $_POST['birthdate'],
				'gender' => $_POST['gender'],
				'purok' => $_POST['purok'],
				'street' => $_POST['street'],
				'kind_of_birth' => $_POST['kind_of_birth'],
				'mother_name' => $_POST['mother_name'],
				'father_name' => $_POST['father_name'],
				'contact_number' => $_POST['contact_number']);
		 		if ($db->update('tbl_child', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Children profile updated successfully!']);
		 		}
		 	}else{
 		 		$data = array('firstname' => $_POST['firstname'],
				'middlename' => $_POST['middlename'],
				'lastname' => $_POST['lastname'],
				'birthdate' => $_POST['birthdate'],
				'gender' => $_POST['gender'],
				'purok' => $_POST['purok'],
				'street' => $_POST['street'],
				'kind_of_birth' => $_POST['kind_of_birth'],
				'mother_name' => $_POST['mother_name'],
				'father_name' => $_POST['father_name'],
				'contact_number' => $_POST['contact_number']);
		 		
		 		if ($db->save('tbl_child', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Children profile saved successfully!']);
		 		}
		 	}
		 }
	break;

	case 'list_sched':
		$array = array();
		$query = "SELECT concat('#00c0ef') as borderColor,concat('#00c0ef') as backgroundColor, concat(count(appointment_id),' ','Scheduled') as title, DATE(appointment_date) as start,DATE(appointment_date) as id from tbl_appointment group by DATE(appointment_date)";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'delete_child':		
		$array = array('child_id' => $_POST['child_id']);
		if ($db->delete('tbl_child', $array)) {
			echo $db->json(['status' => true, 'message' => 'Child successfully deleted!']);
		}
	break;

	case 'save_sched':
		$v = new Validator();

		 $v->make($_POST, [
		 	'child_id' => 'required',
		 	'appointment_date' => 'required',
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	$counter = 0;
		 	foreach ($_POST['child_id'] as $v) {
		 		$data = array('child_id' => $v,
		 					  'description' => $_POST['description'],
		 					  'appointment_date' => $_POST['appointment_date']);

		 		if ($db->save('tbl_appointment', $data)) {
		 			$counter++;
		 		}
		 	}

		 	echo $db->json(['status' => true, 'message' => $counter.' Schedule saved successfully!']);
		 }
	break;

	default:
		# code...
	break;
}

 ?>