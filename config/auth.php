<?php 
session_start();
class Auth{
	public function user($data){
		if (isset($_SESSION[$data])) {
			return $_SESSION[$data];
		}else{
			return '';
		}
	}

	public function check(){
		if (isset($_SESSION['user_id'])) {
			return true;
		}else{
			return false;
		}
	}

	function position(){
		if ($_SESSION['user_type'] == 1) {
			return 'Admin';
		}elseif ($_SESSION['user_type'] == 2) {
			return 'Midwife';
		}elseif ($_SESSION['user_type'] == 3) {
			return 'Health Worker';
		}
	}
	

	function fullname(){
		return ucfirst($_SESSION['firstname']).' '.ucfirst($_SESSION['lastname']);
	}

	public function set($key, $data){
		$_SESSION[$key] = $data;
	}

	public function avatar(){
		return (!empty($_SESSION['avatar']))? $_SESSION['avatar'] : 'webroot/img/avatar.png';
	}

	public function logout(){
		session_destroy();
		return true;
	}
}