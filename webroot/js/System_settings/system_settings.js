 function form_reset(){
    $("#late_config_id").val('');
    document.getElementById('late_config').reset();
  }

  function add_late(){
    $("#modal_add_late_settings").modal('show');
  }

  $("#salaryconfig_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'salaryconfig_id');
          form_reset();
          $("#modal_add_late_settings").modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'salaryconfig_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});


	var tbl_lateconfig;
	function show_lateconfig(){
		if (tbl_lateconfig) {
			tbl_lateconfig.destroy();
		}
		var url = main_path + '/lateconfig/list_lateconfig';
		tbl_lateconfig = $('#tbl_lateconfig').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "from_time",
		"title": "From Time",
	},{
		className: '',
		"data": "end_time",
		"title": "End Time",
	},{
		className: '',
		"data": "deducted_from_time",
		"title": "Deducted from Time",
	},{
		className: 'width-option-1 text-center',
		"data": "late_config_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_lateconfig(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_lateconfig(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#late_config").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'late_config');
					$('#modal_add_late_settings').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
          show_lateconfig();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'late_config');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_lateconfig(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/lateconfig/delete_lateconfig/' + data.late_config_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this Late Deduction Settings?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
          show_lateconfig();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_lateconfig(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#late_config_id').val(data.late_config_id);
		$('#from_time').val(data.from_time);
		$('#end_time').val(data.end_time);
		$('#deducted_from_time').val(data.deducted_from_time);
    add_late();
	}